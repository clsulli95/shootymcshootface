extends Node2D

onready var camera: Camera2D = $Camera2D
onready var mouse_position: Vector2 = Vector2(0, 0)
onready var viewport_resolution: Vector2 = get_viewport_rect().size
onready var edge_boundary_percentage: float = 0.05
onready var y_boundary_offset: float = 0.0
onready var x_boundary_offset: float = 0.0
onready var camera_velocity: float = 5.0

export (float) var ZOOM_MAX = 1.0
export (float) var ZOOM_INCREMENT = 0.25


func _ready():
	print("Viewport Resolution is: ", viewport_resolution)
	print("Viewportr Edge Boundary is: ", edge_boundary_percentage)
	get_tree().get_root().connect("size_changed", self, "handle_resize")
	initialize_boundaries()


func _process(delta):
	process_camera_movement()
	process_zoom()


func _input(event: InputEvent):
	if event is InputEventMouseMotion:
		mouse_position = event.position


func process_zoom():
	if Input.is_action_just_released("wheel_down"):
		camera.zoom.x += ZOOM_INCREMENT
		camera.zoom.y += ZOOM_INCREMENT
	if Input.is_action_just_released("wheel_up") and camera.zoom.x > ZOOM_MAX and camera.zoom.y > ZOOM_MAX:
		camera.zoom.x -= ZOOM_INCREMENT
		camera.zoom.y -= ZOOM_INCREMENT


func process_camera_movement():
	# North boundary
	if mouse_position.y <= y_boundary_offset:
		camera.move_local_y(-camera_velocity)
	# South boundary
	if mouse_position.y >= (viewport_resolution.y - y_boundary_offset):
		camera.move_local_y(camera_velocity)
	# West boundary
	if mouse_position.x <= x_boundary_offset:
		camera.move_local_x(-camera_velocity)
	# East boundary
	if mouse_position.x >= (viewport_resolution.x - x_boundary_offset):
		camera.move_local_x(camera_velocity)


func handle_resize():
	viewport_resolution = get_viewport_rect().size
	print("Viewport resized: ", viewport_resolution)
	initialize_boundaries()


func initialize_boundaries():
	y_boundary_offset = viewport_resolution.y * edge_boundary_percentage
	x_boundary_offset = viewport_resolution.x * edge_boundary_percentage
