extends Node2D

enum Direction { LEFT, RIGHT }

export var move_speed : float
export var bullet_scene : PackedScene

onready var current_direction: int = Direction.RIGHT

func _ready():
	pass


func _process(delta: float):
		orientate_sprite()
		
		$Gun.look_at(get_global_mouse_position())


func _physics_process(delta: float):
	var motion = Vector2.ZERO
	
	if Input.is_action_pressed("up"):
		motion.y -= 1
	if Input.is_action_pressed("down"):
		motion.y += 1
	if Input.is_action_pressed("right"):
		motion.x += 1
	if Input.is_action_pressed("left"):
		motion.x -= 1

	motion = motion.normalized()
	
	if motion == Vector2.ZERO:
		$AnimationTree.get("parameters/playback").travel("idle")
	else:
		$AnimationTree.get("parameters/playback").travel("move")
		$AnimationTree.set("parameters/idle/blend_position", motion)
		$AnimationTree.set("parameters/move/blend_position", motion)
		$KinematicBody2D.move_and_slide(motion * move_speed)

	if Input.is_action_just_pressed("fire"):
		fire()


func fire():
	var bullet = bullet_scene.instance() as Node2D
	bullet.speed = 6500
	get_parent().add_child(bullet)
	bullet.position = $Gun/Muzzle.get_global_position()
	bullet.direction = ($Gun/Position2D.get_global_position() - $Gun/Muzzle.get_global_position()).normalized()
	bullet.rotation = bullet.direction.angle()
	bullet.add_collision_exception_with($KinematicBody2D)


func orientate_sprite():
	# Initialize the direction the sprite is looking at this frame
	var determined_direction : int = Direction.LEFT
	
	# The characters x position in the world, and the mouse's x position
	# in the world
	var g_mouse_x : int = get_global_mouse_position().x
	var g_char_x : int = $KinematicBody2D.get_global_position().x
	
	# We are looking left
	if g_mouse_x <= g_char_x:
		determined_direction = Direction.LEFT
	# Otherwise we are looking right
	else:
		determined_direction = Direction.RIGHT
	
	# Flip sprite assets only if we are looking in a new direction
	if current_direction != determined_direction and determined_direction == Direction.LEFT:
		$KinematicBody2D/Sprite.set_flip_h(true)
		$Gun.set_flip_v(true)
		$Gun/LeftHand.set_flip_v(true)
		$Gun/LeftHand.position.y *= -1.0
		$Gun/RightHand.set_flip_v(true)
		$Gun/RightHand.position.y *= -1.0
		$Gun/Muzzle.position.y *= -1.0
		$Gun/Position2D.position.y *= -1.0
		current_direction = Direction.LEFT
	elif current_direction != determined_direction and determined_direction == Direction.RIGHT:
		determined_direction = Direction.RIGHT
		$KinematicBody2D/Sprite.set_flip_h(false)
		$Gun.set_flip_v(false)
		$Gun/LeftHand.set_flip_v(false)
		$Gun/LeftHand.position.y *= -1.0
		$Gun/RightHand.set_flip_v(false)
		$Gun/RightHand.position.y *= -1.0
		$Gun/Muzzle.position.y *= -1.0
		$Gun/Position2D.position.y *= -1.0
		current_direction = Direction.RIGHT
