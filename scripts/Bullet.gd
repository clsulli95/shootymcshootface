extends KinematicBody2D

export var speed : int = 2000
export var smoke_scene : PackedScene
export var impact_scene : PackedScene
var direction : Vector2 = Vector2.ZERO


func _ready():
	pass


func _process(delta: float):
	var collision_result = self.move_and_collide(direction * speed * delta) as KinematicCollision2D
	
	if collision_result != null:
		var smoke = smoke_scene.instance() as Particles2D
		self.get_parent().add_child(smoke)
		smoke.global_position = collision_result.position
		smoke.rotation = collision_result.normal.angle()
		self.queue_free()

		var impact = impact_scene.instance() as Node2D
		get_parent().add_child(impact)
		impact.global_position = collision_result.position
		impact.rotation = collision_result.normal.angle()
		queue_free()
